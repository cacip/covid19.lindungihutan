<!DOCTYPE html>
<html lang="id">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="LindungiHutan sebagai salah satu situs crowdfunding di Indonesia berharap semoga ikhtiar kecil ini dapat membantu kita yang aktivitasnya terbatas dari dalam rumah untuk dapat bersama melawan virus COVID-19 sehingga kita dapat kembali beraktivitas dan mewujudkan Indonesia yang lebih hijau dan tak bisa kiranya kita LindungiHutan jika kita abai untuk #LindungiDiri.">
    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
    <link rel="canonical" href="https://covid19.lindungihutan.com/" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Waspada Covid19 | LindungiHutan" />
    <meta property="og:description" content="LindungiHutan sebagai salah satu situs crowdfunding di Indonesia berharap semoga ikhtiar kecil ini dapat membantu kita yang aktivitasnya terbatas dari dalam rumah untuk dapat bersama melawan virus COVID-19 sehingga kita dapat kembali beraktivitas dan mewujudkan Indonesia yang lebih hijau dan tak bisa kiranya kita LindungiHutan jika kita abai untuk #LindungiDiri." />
    <meta property="og:url" content="https://covid19.lindungihutan.com/" />
    <meta property="og:site_name" content="covid19.lindungihutan.com" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:description" content="LindungiHutan sebagai salah satu situs crowdfunding di Indonesia berharap semoga ikhtiar kecil ini dapat membantu kita yang aktivitasnya terbatas dari dalam rumah untuk dapat bersama melawan virus COVID-19 sehingga kita dapat kembali beraktivitas dan mewujudkan Indonesia yang lebih hijau dan tak bisa kiranya kita LindungiHutan jika kita abai untuk #LindungiDiri." />
    <meta name="twitter:title" content="Waspada Covid19 | LindungiHutan" />
    <meta name="twitter:site" content="@lindungihutan" />
    <link rel='dns-prefetch' href='//www.gstatic.com' />
    <link rel='dns-prefetch' href='//www.googletagmanager.com' />
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel='stylesheet' id='font-awesome-css' href='https://covid19.lindungihutan.com/css/font-awesome.min.css' type='text/css' media='all' />
    
<!--    google signin-->
    <meta name="google-signin-client_id" content="798842087881-c9r27kn8q3fuvfs0oq3paf5hdeb9tl51.apps.googleusercontent.com">
<!--end google signin-->
    
    <link rel="shortcut icon" href="https://lindungihutan.com/public/img-redesign/logo_lindungihutan.png" type="image/png"/>

    <!-- bootstrap -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link src="css/font-awesome.min.css" rel="stylesheet" type="text/css" >

    <!-- font -->
    <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">

    
    <title>LindungiHutan COVID-19</title>

    <!-- custom style -->
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/style-responsive.css" rel="stylesheet" type="text/css" />
    <link href="css/toastr/toastr.css" rel="stylesheet" type="text/css" />

    <style>
        .box-campaign.promotion{
            min-height: 200px;
            margin-top: 30px;
            padding-bottom: 0;
        }
        img.img-campaign.promo{
            border-radius: 5px;
        }

        /*promokaos*/
        #area-float-right {
            width: 220px;
            position: fixed;
            left: 20px;
            bottom: 20px;
            z-index: 9;
        }
        .fl-builder-content *, .fl-builder-content *:before, .fl-builder-content *:after {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        #area-float-right .img-float {
            width: 100%;
        }
        .fl-module img {
            max-width: 100%;
        }
        .fl-builder-content *, .fl-builder-content *:before, .fl-builder-content *:after {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        /*endpromokaos*/
        .dropdown-mobile {
            display: none;
        }

        .active {
            display: block;
        }

        .Mnavbar {
            background-color: #24bd6e;
            color: #fff;
            width: 100%;
        }

        .dropdown-content {
            /* display: none; */
            margin-top:50px;
            margin-left:180px;
            position: absolute;
            background-color: #f9f9f9;
            min-width: 200px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            /* padding: 12px 16px; */
            z-index: 1;
        }

        .dropdown-content a {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }

        .dropdown-content a:hover {background-color: #f1f1f1}

        .dropdown:hover .dropdown-content {
            display: block;
        }

        .dropdown:hover .dropbtn {
            background-color: #3e8e41;
        }

        .val-summary {
            border-radius: 12px;
            background-color: #ccc;
            padding: 5px;
            margin: 20px;
        }

        .yellow {
            background-color: #FFD600;
        }

        .orange {
            background-color: #FFA300;
        }

        .green {
            background-color: #46CB3F;
        }

        .red {
            background: rgba(253, 50, 50, 0.81);
        }

        .blue {
            background-color: #0386FF;
        }

        .blue-light {
            background-color: #359EFF;
        }

        .blue-lighter {
            background-color: #83C9FF;
        }

        .contribute {
            border: 1px solid #000000;
            border-radius: 25px;
        }


        .button-icon {
            margin: auto;
            margin-top: 40px;
            margin-bottom: 40px;
            vertical-align: center;
        }

        .btn-donasi {
            border-radius: 25px;
            background-color: #29C367;
            color: #fff;
        }

        @media (min-width: 457px) {
            .icon-tombol {
                max-width: 150px;
            }

            .icon-unduh {
                max-width: 120px;
                height: 90px;
            }

            .tombol-icon {
                margin: 10px;
                padding: 5px;
            }

            .mapIframe {
                height: 600px; 
                width: 100%;
            }
        }
        @media (max-width: 456px) {
            .icon-tombol {
                max-width: 110px;
                height: 143px;
            }

            .icon-unduh {
                max-width: 90px;
            }

            .tombol-icon {
                margin: auto;
                padding: 5px;
            }

            .mapIframe {
                height: 500px; 
                width: 100%;
            }
        }
        

    </style>
</head>
<body style="background-color: #fff;">
    
<div class="navbar hidden-xs" id="top-nav">
    <div class="container-fluid">
        <ul class="nav navbar-nav navbar-right nav-top">
            <li><a href="https://bit.ly/sekolahhutan02" target="_blank">SekolahHutan</a></li>
            <li><a href="https://lindungihutan.com/jadigini" target="_blank">Merchandise</a></li>
            <li><a href="https://blog.lindungihutan.com/" target="_blank">Blog</a></li>
            <li><a href="https://lindungihutan.com/faq">FAQ</a></li>
        </ul>
    </div>
</div>
<div class="navbar navbar-default" id="nav" style="padding-top: 5px;">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand" href="https://lindungihutan.com/">
          <img src="https://lindungihutan.com/public/img-redesign/logo_lindungihutan.png" id="brand-img" alt="" style="display:inline"> <span style="display:inline; position:relative; bottom:10px; color:#045f50; font-weight:bold">LindungiHutan</span>
        </a>
    </div>

    <div class="hidden-xs">
      <!-- <ul class="nav navbar-nav navbar-right">
        <li><a href="https://lindungihutan.com/kampanyealam">Kampanye Alam</a></li>
        <li><a href="https://lindungihutan.com/buatkampanye">Buat Kampanye</a></li>
        <li class="hidden-sm"><a href="https://lindungihutan.com/aktifitaskami">Aktifitas Kami</a></li>
        <li class="hidden-sm"><a href="https://lindungihutan.com/masuk" class="btn btn-primer-outline">Login/Register</a></li>
      </ul> -->
      <ul class="nav navbar-nav navbar-right">

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Projek <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="https://lindungihutan.com/satuhutan">SatuHutan</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="https://lindungihutan.com/hutanmerdeka">HutanMerdeka</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="https://lindungihutan.com/rawatbumi">RawatBumi</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="https://lindungihutan.com/kampanyealam">Kampanye Alam</a></li>
            <li role="separator" class="divider"></li>
            <li><a style="color:#045f50; font-weight:bold; " href="https://lindungihutan.com/daftar_kampanye">Buat Kampanye</a></li>
          </ul>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Partner <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="https://lindungihutan.com/komunitas">Relawan</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="https://lindungihutan.com/partner">Kerjasama</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="https://lindungihutan.com/csr">CSR </a></li>
          </ul>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tentang Kami <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="https://lindungihutan.com/aktifitaskami">Aktifitas</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="https://lindungihutan.com/profil">Profil</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="https://lindungihutan.com/team">Team</a></li>
          </ul>
        </li>

      </ul>
    </div>
      <div class="pull-right hidden-lg hidden-md hidden-sm">
          <div id="nav-icon3">
              <span></span>
              <span></span>
              <span></span>
              <span></span>
            </div>
      </div>

  </div><!-- /.container-fluid -->
</div>

<div class="col-xs-8 col-xs-offset-4 text-right" id="mobile-navbar">
    <ul id="list-mobile-navbar">
        <li><a href="{{ 'https://api.whatsapp.com/send?phone='.$settings->phone_company.'&text=Halo LindungiHutan, saya ingin bertanya tentang ..' }}" target="_blank" >Bantuan</a></li>
        <li><a href="https://lindungihutan.com/produk">Merchandise</a></li>
        <li><a href="https://lindungihutan.com/partner">Kerjasama</a></li>
        <li><a href="https://lindungihutan.com/komunitas">Relawan</a></li>
        <li><a href="https://blog.lindungihutan.com/" target="_blank">Blog</a></li>
        <li><a href="https://lindungihutan.com/faq">FAQ</a></li>
        <li><a href="https://lindungihutan.com/kebijakan">Kebijakan Privasi</a></li>
        <li><a href="https://lindungihutan.com/profil">Tentang Kami</a></li>
    </ul>

</div>
<!-- end of navbar -->


<div class="wrap-loader">
    <img class="img" id="preload" src="https://lindungihutan.com/public/img-redesign/loaderdaun.gif">
</div>

<div class="row section">
    <div class="container-read">
        <!-- <h1 class="title">Saatnya Peduli Dan Saling Melindungi</h1>
        <h4 class="text-center" style="padding-bottom:20px">Mari bersama #LindungiDiri #DiRumahAja</h4> -->
        <!-- <img src="image/bannerWeb.svg" class="img-responsive"/> -->
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <h3 style="color: #2980C3; font-weight: bold;">#lindungidiri dari Covid19</h3>
            <p style="padding-bottom:20px">LindungiHutan sebagai salah satu situs crowdfunding di Indonesia, berusaha membantu untuk dapat bersama melawan virus COVID-19. Sehingga kita dapat kembali beraktivitas dan mewujudkan Indonesia yang lebih hijau lagi.</p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
            <img src="image/banner.svg" class="img-responsive"/>
        </div>
    </div>
</div>

<?php

# Author: Ghazif Adeem 
# Github: https://github.com/Ghazif-Adeem

$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

curl_setopt($ch, CURLOPT_URL, 'https://covid19.mathdro.id/api/countries/indonesia');
$result = curl_exec($ch);
$data = json_decode($result, true);

// curl_setopt($ch, CURLOPT_URL, 'https://covid19.mathdro.id/api');
// $MainApi = curl_exec($ch);
// $totalStatistics = json_decode($MainApi, true);

?>

<div class="row ">
    <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 20px; margin-top: 20px;">
            <h3 class="bold">Jumlah Kasus di Indonesia Saat ini</h3>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
            <div class="val-summary yellow">
                <h1 style="color: #fff; text-align: center; font-size: 56px;"><?php echo number_format($data['confirmed']['value']) ?></h1>
                <h3 class="bold" style="color: #fff; text-align: center;">Terkonfirmasi</h3>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
            <div class="val-summary orange">
                <h1 style="color: #fff; text-align: center; font-size: 56px;">
                    <?php echo number_format(($data['confirmed']['value'])-($data['recovered']['value']+$data['deaths']['value'])) ?>
                </h1>
                <h3 class="bold" style="color: #fff; text-align: center;">Dalam Perawatan</h3>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
            <div class="val-summary green">
                <h1 style="color: #fff; text-align: center; font-size: 56px;"><?php echo $data['recovered']['value'] ?></h1>
                <h3 class="bold" style="color: #fff; text-align: center;">Sembuh</h3>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
            <div class="val-summary red">
                <h1 style="color: #fff; text-align: center; font-size: 56px;"><?php echo $data['deaths']['value'] ?></h1>
                <h3 class="bold" style="color: #fff; text-align: center;">Meninggal</h3>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-bottom: 20px; margin-top: 20px;">
            <a href="#mapCovid" id="button-mapCovid" style="color: #2980C3; font-weight: bold; font-size: 21px;">Lihat statistik harian ></a>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-bottom: 20px; margin-top: 20px;">
            <p style="text-align: right;">*Update terakhir @ <?php echo date('d-M-Y H:i:s', strtotime($data['lastUpdate'])) ?></h5>
        </div>
    </div>
</div>

<div class="row">
    <div class="container text-center">
        <div class="" id="mapCovid" style="margin-top:40px; display:none; font-size:1.3em">
          <iframe loading="lazy" scrolling="no" frameborder="0" class="mapIframe" src="https://experience.arcgis.com/experience/bf4eb5d76e98423c865678e32c8937d4"></iframe>
        </div>
    </div>
</div>

<div class="row ">
    <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 20px; margin-top: 20px;">
            <h3 class="bold">Jumlah Donasi Terkumpul</h3>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <div class="val-summary blue">
                <h1 style="color: #fff; text-align: center; font-size: 56px;">115 </h1>
                <h3 class="bold" style="color: #fff; text-align: center;">Produk Terjual</h3>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="val-summary blue-light">
                <h1 style="color: #fff; text-align: center; font-size: 56px;">
                    Rp 6.716.00
                </h1>
                <h3 class="bold" style="color: #fff; text-align: center;">Donasi Terkumpul</h3>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <div class="val-summary blue-lighter">
                <a href="https://docs.google.com/document/d/1ymg55GpexmQs7bwsNe8lRhkwiz1yayTOX_sBHkqk67g/edit?usp=sharing" target="_blank">
                    <img src="image/logo-png-download.svg" class="img-responsive icon-unduh center-block" />
                    <h3 class="bold" style="color: #fff; text-align: center;">Download Report</h3>
                </a>                
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-bottom: 20px; margin-top: 20px;">
            <a style="color: #2980C3; font-weight: bold; font-size: 21px;">&nbsp;</a>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-bottom: 20px; margin-top: 20px;">
            <p style="text-align: right;">Update sampai 29 April 2020 pukul 15.00</h5>
        </div>
    </div>
</div>


<div class="row">
    <div class="container-read">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 20px; margin-top: 20px;">
            <h3 class="bold">Saatnya  peduli dan saling melindungi</h3>
            <p>Tiap pembelian satu merchandise LindungiHutan, kamu turut serta berdonasi sebesar Rp 50.000 untuk menanggulangi dampak wabah Covid19. Silahkan pilih merchandise favoritmu dan sebarkan pesan untuk melindungi hutan ke sekelilingmu. </p>
        </div>
        <div class="contribute col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <!-- <div class="tombol-icon">
                    <a href="https://lindungihutan.orderonline.id/jadigini" target="_blank">
                        <img src="image/kaos-jadigini.svg" class="img-responsive icon-tombol center-block" />
                        <h4 class="bold" style="text-align: center;color: #8D8D8D;">T Shirt</h4>
                        <p style="text-align: center;color: #8D8D8D; font-size: 10px;">#JadiGini</p>
                    </a>
                </div> -->
                <div class="tombol-icon">
                    <a href="viewDetail.php?item=1" target="_blank">
                        <img src="image/kaos-jadigini.svg" class="img-responsive icon-tombol center-block" />
                        <h4 class="bold" style="text-align: center;color: #8D8D8D;">T Shirt</h4>
                        <p style="text-align: center;color: #8D8D8D; font-size: 10px;">#JadiGini</p>
                    </a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <div class="tombol-icon">
                    <a href="viewDetail.php?item=2" target="_blank">
                        <img src="image/kaos-rawatbumi.svg" class="img-responsive icon-tombol center-block" />
                        <h4 class="bold" style="text-align: center;color: #8D8D8D;">T Shirt</h4>
                        <p style="text-align: center;color: #8D8D8D; font-size: 10px;">#RawatBumi</p>
                    </a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <div class="tombol-icon">
                    <a href="viewDetail.php?item=3" target="_blank">
                        <img src="image/kaos-lh.svg" class="img-responsive icon-tombol center-block" />
                        <h4 class="bold" style="text-align: center;color: #8D8D8D;">T Shirt</h4>
                        <p style="text-align: center;color: #8D8D8D; font-size: 10px;">#LindungiHutan</p>
                    </a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <div class="tombol-icon">
                    <a href="viewDetail.php?item=4" target="_blank">
                        <img src="image/hoodie.svg" class="img-responsive icon-tombol center-block" />
                        <h4 class="bold" style="text-align: center;color: #8D8D8D;">Hoodie</h4>
                        <p style="text-align: center;color: #8D8D8D; font-size: 10px;">#JadiGini</p>
                    </a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <div class="tombol-icon">
                    <a href="viewDetail.php?item=5" target="_blank">
                        <img src="image/kaos-planetb.svg" class="img-responsive icon-tombol center-block" />
                        <h4 class="bold" style="text-align: center;color: #8D8D8D;">T Shirt</h4>
                        <p style="text-align: center;color: #8D8D8D; font-size: 10px;">#NoPlanetB</p>
                    </a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <div class="tombol-icon">
                    <a href="viewDetail.php?item=6" target="_blank">
                        <img src="image/kaos-hm.svg" class="img-responsive icon-tombol center-block" />
                        <h4 class="bold" style="text-align: center;color: #8D8D8D;">T Shirt</h4>
                        <p style="text-align: center;color: #8D8D8D; font-size: 10px;">HutanMerdeka</p>
                    </a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <div class="tombol-icon">
                    <a href="viewDetail.php?item=7" target="_blank">
                        <img src="image/kaos-family.svg" class="img-responsive icon-tombol center-block" />
                        <h4 class="bold" style="text-align: center;color: #8D8D8D;">T Shirt</h4>
                        <p style="text-align: center;color: #8D8D8D; font-size: 10px;">ForestFamily</p>
                    </a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <div class="tombol-icon">
                    <a href="viewDetail.php?item=8" target="_blank">
                        <img src="image/straw.svg" class="img-responsive icon-tombol center-block" />
                        <h4 class="bold" style="text-align: center;color: #8D8D8D;">Straw</h4>
                        <p style="text-align: center;color: #8D8D8D; font-size: 10px;">LindungiHutan</p>
                    </a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <div class="tombol-icon">
                    <a href="viewDetail.php?item=9" target="_blank">
                        <img src="image/totebag.svg" class="img-responsive icon-tombol center-block" />
                        <h4 class="bold" style="text-align: center;color: #8D8D8D;">Totebag</h4>
                        <p style="text-align: center;color: #8D8D8D; font-size: 10px;">LindungiHutan</p>
                    </a>
                </div>
            </div>
            <!-- <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <div class="tombol-icon">
                    <a href="viewDetail.php?item=10" target="_blank">
                        <img src="image/tumbler.svg" class="img-responsive icon-tombol center-block" />
                        <h4 class="bold" style="text-align: center;color: #8D8D8D;">Tumbler</h4>
                        <p style="text-align: center;color: #8D8D8D; font-size: 10px;">LindungiHutan</p>
                    </a>
                </div>
            </div> -->
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <div class="tombol-icon">
                    <a href="viewDetail.php?item=12" target="_blank">
                        <img src="image/kaos-planetblawas.svg" class="img-responsive icon-tombol center-block" />
                        <h4 class="bold" style="text-align: center;color: #8D8D8D;">T Shirt</h4>
                        <p style="text-align: center;color: #8D8D8D; font-size: 10px;">Klasik #NoPlanetB</p>
                    </a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <div class="tombol-icon">
                    <a href="viewDetail.php?item=11" target="_blank">
                        <img src="image/topi2.svg" class="img-responsive icon-tombol center-block" />
                        <h4 class="bold" style="text-align: center;color: #8D8D8D;">Topi Rimba</h4>
                        <p style="text-align: center;color: #8D8D8D; font-size: 10px;">#Teduhi</p>
                    </a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <div class="tombol-icon">
                    <a href="viewDetail.php?item=13" target="_blank">
                        <img src="image/bibit.svg" class="img-responsive icon-tombol center-block" />
                        <h4 class="bold" style="text-align: center;color: #8D8D8D;">Benih</h4>
                        <p style="text-align: center;color: #8D8D8D; font-size: 10px;">#TanamDiRumah</p>
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="button-icon">
                    <a href="https://lindungihutan.com/sedekahpohon" target="_blank">
                        <button class="btn-donasi btn-lg " pills>Donasi</button>
                    </a>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="row">
    <div class="container-read">
        <h3>#lindungidiri dari Covid19</h3>
        <div style="margin-top: 30px;">
            <!-- LightWidget WIDGET -->
            <!-- <script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/a714630cb17c5f6aad27704c69263bef.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width:100%;border:0;overflow:hidden;"></iframe> -->
        </div>
        <p>Saatnya peduli dan saling melindungi </p>
        <p>Merebaknya wabah COVID-19 membuat beberapa aktivitas kita terbatas, sebagaimana anjuran pemerintah dan pakar, kegiatan yang melibatkan massa harus dikurangi karena mudahnya virus ini menular lewat kontak fisik. Begitu pula dengan beberapa rencana penanaman LindungiHutan dan antusiasme para pemuda untuk turut menghijaukan Indonesia yang juga harus tertunda sampai kondisi kembali pulih.</p>
        <p>Sementara proses penanaman bersama LindungiHutan yang terhambat dan juga produk merchandise kami yang menumpuk tinggi, tak layak kami menawarkan donasi untuk penanaman. Maka hadirlah campaign #LindungiDiri yang menawarkan produk merchandise LindungiHutan dengan harga khusus yang dibundling dengan donasi Rp 50.000 setiap penjualan untuk turut membantu penanganan kasus corona.</p>
        <p>Donasi akan kami serahkan kepada Yayasan Graisena (Gerakan Indonesia Sadar Bencana) yang dari rekam jejaknya terbukti berkompeten dalam turut meredakan masalah virus COVID-19 dengan melakukan desinfeksi di beberapa titik dan menanggung pengeluaran kepala keluarga yang positif corona, seperti yang nampak pada situs (<a href="https://graisena.or.id/" target="_blank">https://graisena.or.id/</a>) dan sosial media <a href="https://instagram.com/graisenacs?igshid=7pzspjniq200" target="_blank"> Instagram</a> </p>
        <p>Tenang saja, pengiriman paket tidak menularkan virus, COVID-19 hanya menyebar dari kontak langsung manusia ke manusia. Menurut WHO, kecil kemungkinan barang komersial terkontaminasi dari orang yang terinfeksi dan kecil juga risiko tertular virus corona baru dari paket yang sudah berpindah, berada dalam perjalanan, dan terpapar kondisi serta suhu yang berbeda-beda.</p>
        <p>Selalu jaga kesehatan, cuci tangan setelah beraktifitas, kurangi berada dalam kerumunan. Periksakan diri ke rumah sakit yang telah ditunjuk pemerintah ketika mengalami gejala demam di atas 38 derajat celcius dan mengalami batuk kering atau setelah mengunjungi daerah terpapar dan berinteraksi dengan orang yang positif COVID-19. Sejenak kita kembali ke rumah bersama keluarga dan sementara dapat #TanamDariRumah bersama LindungiHutan.</p>
        <p>LindungiHutan sebagai salah satu situs crowdfunding di Indonesia berharap semoga ikhtiar kecil ini dapat membantu kita yang aktivitasnya terbatas dari dalam rumah untuk dapat bersama melawan virus COVID-19 sehingga kita dapat kembali beraktivitas dan mewujudkan Indonesia yang lebih hijau dan tak bisa kiranya kita LindungiHutan jika kita abai untuk #LindungiDiri.</p>
        <p>Mari bersama #LindungiDiri #DiRumahAja</p>
        
        
    </div>
    
</div>

<!-- <div class="row ">
    <div class="container">

        <?php if(!empty($data['confirmed'])): ?>
            <h4>Statistics of Indonesia</h4>
            <ul>
                <li>Infected - <?php echo number_format($data['confirmed']['value']) ?></li>
                <li>Recovered - <?php echo number_format($data['recovered']['value']) ?></li>
                <li>Death(s) - <?php echo number_format($data['deaths']['value']) ?></li>
                <li>Last Updated @ <?php echo $data['lastUpdate'] ?></li>
            </ul>
            <hr>
        <?php endif; ?>
    </div>
</div> -->


<!-- end of campaign -->
<!-- modal intro -->
<div class="modal fade" id="modalIntro" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="myModalLabel" class="bold" style="font-weight: bold;">Halo..!</h4>
      </div> -->
      <div class="modal-body text-center">
          <p>Ini adalah halaman dukungan dari LindungiHutan.com untuk membantu masalah COVID-19. <br> Kami membantu menyalurkan bantuan donasi dan keuntungan dari penjualan produk-produk kami melalui Yayasan Gerakan Indonesia Sadar Bencana <a href="https://graisena.or.id" style="color:#045f50; font-weight: bold;">Graisena.or.id</a>.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- end modal -->

    <div class="back-to-top" style="background-color: transparent;">
        <img src="image/gototop.png" class="img-responsive" style="width: 60px;">
    </div>

<div class="row" id="footer">
    <div class="container">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div>
                <a class="img-footer" href="https://lindungihutan.com" style="height: 35px;">
                    <img src="https://lindungihutan.com/public/img-redesign/logo_lindungihutan.png" id="brand-img" alt="" style="display:inline; ">
                    <span style="display:inline; position:relative; bottom:10px;  color:#045f50; font-weight:bold"> LindungiHutan </span>
                </a>
            </div>
            <p>
                <div style="font-weight: bold; font-size: 15px;">Website Penggalangan Dana Online untuk Konservasi Hutan dan Lingkungan.</div>
                <div>Yayasan Lindungi Hutan resmi berbadan hukum dan memiliki Izin Pengumpulan Sumbangan pada SK Kemensos No. 252/HUK-PS/2020.
                </div>
            </p>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6">
            <h4 style="margin-top:0"><b>Tentang Kami</b></h4>
            <ul class="link-footer" style="">
                <li><a href="https://lindungihutan.com/carakerja">Cara Kerja </a></li>
                <li><a href="https://lindungihutan.com/profil">Profil </a></li>
                <li><a href="https://lindungihutan.com/team">Team </a></li>
            </ul>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6">
            <h4 style="margin-top:0"><b></b></h4>
            <ul class="link-footer" style="margin-top:30px;">
                <li><a href="https://lindungihutan.com/kebijakan">Kebijakan dan Privasi </a></li>
                <li><a href="https://lindungihutan.com/faq">FAQ </a></li>
                <li><a href="https://lindungihutan.com/faq"> </a></li>
            </ul>
        </div>
        <div class="col-md-2 col-sm-6 col-xs-6">
            <h4 style="margin-top:0"><b></b></h4>
            <ul class="link-footer" style="margin-top:30px;">
                <li><a href="https://lindungihutan.com/partner">Mitra Alam</a></li> 
                <li><a href="https://lindungihutancsr.typeform.com/to/mBehaO" target="_blank"> Menjadi Mitra</a></li>
                <li><a href="https://lindungihutancsr.typeform.com/to/mBehaO" target="_blank"> </a></li>
            </ul>
        </div>
        <div class="col-md-2 col-sm-6 col-xs-6">
            <h4 style="margin-top:0"><b></b></h4>
            <ul class="link-footer" style="margin-top:30px;">
                <li><a href="https://lindungihutan.com/komunitas">Relawan </a></li>
                <li><a href="https://lindungihutan.com/sayasiap"> Menjadi Relawan</a></li>
                <li><a href="https://lindungihutan.com/sayasiap"> </a></li>
            </ul>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <ul id="socmed">
                <li>
                    <a href="https://www.facebook.com/lindungihutandotcom/" target="_blank">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 486.392 486.392" style="enable-background:new 0 0 486.392 486.392;" xml:space="preserve" width="28px" height="28px">
                        <path class="fb-lh" d="M395.193,0H91.198C40.826,0,0,40.826,0,91.198v303.995c0,50.372,40.826,91.198,91.198,91.198     h303.995c50.372,0,91.198-40.827,91.198-91.198V91.198C486.392,40.826,445.565,0,395.193,0z M306.062,243.165l-39.854,0.03     l-0.03,145.917h-54.689V243.196H175.01v-50.281l36.479-0.03l-0.061-29.609c0-41.039,11.126-65.997,59.431-65.997h40.249v50.311     h-25.171c-18.817,0-19.729,7.022-19.729,20.124l-0.061,25.171h45.234L306.062,243.165z" fill="#cccccc"/>
                    </svg>
                    </a>
                </li>
                <li>
                    <a href="https://twitter.com/LindungiHutan" target="_blank">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 486.392 486.392" style="enable-background:new 0 0 486.392 486.392;" xml:space="preserve" width="28px" height="28px">
                        <path class="tw-lh" d="M395.193,0H91.198C40.826,0,0,40.826,0,91.198v303.995c0,50.372,40.826,91.198,91.198,91.198     h303.995c50.372,0,91.198-40.827,91.198-91.198V91.198C486.392,40.826,445.565,0,395.193,0z M364.186,188.598l0.182,7.752     c0,79.16-60.221,170.359-170.359,170.359c-33.804,0-65.268-9.91-91.776-26.904c4.682,0.547,9.454,0.851,14.288,0.851     c28.059,0,53.868-9.576,74.357-25.627c-26.204-0.486-48.305-17.814-55.935-41.586c3.678,0.669,7.387,1.034,11.278,1.034     c5.472,0,10.761-0.699,15.777-2.067c-27.39-5.533-48.031-29.7-48.031-58.701v-0.76c8.086,4.499,17.297,7.174,27.116,7.509     c-16.051-10.731-26.63-29.062-26.63-49.825c0-10.974,2.949-21.249,8.086-30.095c29.518,36.236,73.658,60.069,123.422,62.562     c-1.034-4.378-1.55-8.968-1.55-13.649c0-33.044,26.812-59.857,59.887-59.857c17.206,0,32.771,7.265,43.714,18.908     c13.619-2.706,26.448-7.691,38.03-14.531c-4.469,13.984-13.953,25.718-26.326,33.135c12.069-1.429,23.651-4.682,34.382-9.424     C386.073,169.659,375.889,180.208,364.186,188.598z" fill="#cccccc"/>
                    </svg>
                    </a>
                </li>
                <li>
                    <a href="https://www.youtube.com/channel/UC5QbLWq-CDaVprd8RGONnpw" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 504 504" style="enable-background:new 0 0 504 504;" xml:space="preserve" width="28px" height="28px">
                            <polygon class="yt-lh" points="219.6,202.4 219.6,294.4 304.4,248.8   " fill="#cccccc"/>
                            <path class="yt-lh" d="M377.6,0H126C56.8,0,0,56.8,0,126.4V378c0,69.2,56.8,126,126,126h251.6c69.6,0,126.4-56.8,126.4-126.4V126.4    C504,56.8,447.2,0,377.6,0z M408,264.4c0,26.4-2.4,53.2-2.4,53.2s-2.8,22.4-12,32.4c-12,13.2-25.2,13.2-31.2,14    c-44,3.2-110,3.6-110,3.6s-82-1.2-107.2-3.6c-6.8-1.2-22.8-0.8-34.8-14c-9.6-10-12-32.4-12-32.4S96,290.8,96,264.4v-24.8    c0-26.4,2.4-53.2,2.4-53.2s2.8-22.4,12-32.4c12-13.2,25.2-13.6,31.2-14.4C186,136.4,252,136,252,136s66,0.4,110,3.6    c6,0.8,19.6,1.2,31.6,14c9.6,10,12,32.8,12,32.8s2.4,26.8,2.4,53.2V264.4z" fill="#cccccc"/>
                        </svg>
                    </a>
                </li>
                <li>
                    <a href="https://www.instagram.com/lindungihutan/" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 504.4 504.4" style="enable-background:new 0 0 504.4 504.4;" xml:space="preserve" width="28px" height="28px">
                            <path class="ig-lh" d="M296.8,219.8c-10-14-26.4-23.2-44.8-23.2c-18.4,0-34.8,9.2-44.8,23.2c-6.4,9.2-10.4,20.4-10.4,32.4    c0,30.4,24.8,55.2,55.2,55.2c30.4,0,55.2-24.8,55.2-55.2C307.2,240.2,303.2,229,296.8,219.8z" fill="#cccccc"/>
                            <path class="ig-lh" d="M331.6,220.2c4,8,6.4,20.8,6.4,32c0,47.2-38.4,86-86,86c-47.6,0-86-38.4-86-86c0-11.6,2.4-24,6.4-32H124v128.4    c0,16.8,14.8,31.6,31.6,31.6h192.8c16.8,0,31.6-14.8,31.6-31.6V220.2H331.6z" fill="#cccccc"/>
                            <polygon class="ig-lh" points="365.6,131.4 319.2,131.4 319.2,184.6 372,184.6 372,138.2 372,131   " fill="#cccccc"/>
                            <path class="ig-lh" d="M377.6,0.2H126.4C56.8,0.2,0,57,0,126.6v251.6c0,69.2,56.8,126,126.4,126H378c69.6,0,126.4-56.8,126.4-126.4V126.6    C504,57,447.2,0.2,377.6,0.2z M408,219.8L408,219.8l0,128.8c0,33.6-26,59.6-59.6,59.6H155.6c-33.6,0-59.6-26-59.6-59.6V219.8v-64    c0-33.6,26-59.6,59.6-59.6h192.8c33.6,0,59.6,26,59.6,59.6V219.8z" fill="#cccccc"/>
                        </svg>
                    </a>
                </li>
            </ul>
        </div>

        <div class="col-xs-12" style="margin-top:20px;">
            LindungiHutan &copy; 2020 - made with <img class="img img-responsive lazy" alt="love-lindungihutan" src="https://lindungihutan.com/public/img-redesign/like.svg" style="display:inline">
        </div>
    </div>
</div>
<!-- end of footer -->


<!-- script -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/redesign/jquery.lazy.min.js" type="text/javascript"></script>

<script type="text/javascript">

    $(window).bind("scroll", function() {
        if ($(window).scrollTop() > 40) 
        {
            $('#nav').delay(200).addClass("fix-top");
            $('.top').css('margin-top','75px');
        } 
        else 
        {
            $('#nav').removeClass("fix-top");
            $('.top').css('margin-top','0');
        }
    });

    $(document).ready(function(){

        $('#button-mapCovid').click(function() {
            $('#mapCovid').slideToggle();
        });
        $('#nav-icon3').click(function(){
            $(this).toggleClass('open');
            $('#mobile-navbar').slideToggle();
        });

        $('#modalIntro').modal('show');

    });


    $('button[type=submit], input[type=submit], a[type=submit]').not('.btn_search').click(function(){
        $('.wrap-loader').show();
    });
    
</script>

<script type="text/javascript">
    jQuery(document).ready(function() {
        var offset = 220;
        var duration = 500;
        jQuery(window).scroll(function() {
            if (jQuery(this).scrollTop() > offset) {
                jQuery('.back-to-top').fadeIn(duration);
            } else {
                jQuery('.back-to-top').fadeOut(duration);
            }
        });

        jQuery('.back-to-top').click(function(event) {
            event.preventDefault();
            jQuery('html, body').animate({scrollTop: 0}, duration);
            return false;
        })
    });
</script>

<script type="text/javascript">
    $(function() {
        $('.lazy').Lazy();
    });
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116293979-4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-116293979-4');
</script>

<!--google signin-->
<script src="https://apis.google.com/js/platform.js" async defer></script>
